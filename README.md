# Required
node v>=16.0.0
npm install -g yarn 
yarn 1.22.10

# Install
yarn install

# Run
yarn start
# Build
yarn build
