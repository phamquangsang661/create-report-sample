import ReactToPrint from 'react-to-print';
import Report from './component/Report';
import React, { useRef, useState } from 'react'
//* Import css
import './css/report.css'
import './App.css';

function App() {

  //* State
  const [isPrint, setIsPrint] = useState(false)

  //* Ref
  const componentRef = useRef();

  return (
    <div className="App">
      <ReactToPrint
        onAfterPrint={() => { setIsPrint(false) }}
        documentTitle="Phụ lục III"
        onBeforeGetContent={() => {
          return new Promise((resolve) => {
            setIsPrint(true)
            setTimeout(() => {
              resolve(true)
            }, 200)
          })
        }}
        trigger={() =>
          <button   >In ra</button>
        }
        content={() => componentRef.current}
      />
      <Report isPrint={isPrint} ref={componentRef}></Report>
    </div>
  );
}

export default App;
